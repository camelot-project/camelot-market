(defproject camelot-market "0.1.0-SNAPSHOT"
  :description "Common code for Camelot and its components."
  :url "http://gitlab.com/camelot-project/camelot-market"
  :license {:name "AGPL v3"
            :url "https://www.gnu.org/licenses/agpl-3.0.en.html"}
  :plugins [[lein-tools-deps "0.4.1"]
            [lein-cljsbuild "1.1.7"]
            [lein-environ "1.1.0"]
            [lein-doo "0.1.7"]]
  :middleware [lein-tools-deps.plugin/resolve-dependencies-with-deps-edn]
  :lein-tools-deps/config {:config-files [:install :user :project]}
  :source-paths ["src"]
  :test-paths ["test"]
  :cljsbuild {:builds [{:id "build"
                        :source-paths ["src"]
                        :compiler {:target :nodejs
                                   :output-to  "target/cljs/node/camelot-market.js"
                                   :output-dir "target/cljs/node/out"
                                   :main camelot.market.config
                                   :optimizations :none
                                   :process-shim false}}
                       {:id "test-cljs"
                        :source-paths ["src" "test"]
                        :compiler {:target :nodejs
                                   :output-to  "target/cljs/test-node/test-cljs.js"
                                   :output-dir "target/cljs/test-node/out"
                                   :main camelot.market.test-runner
                                   :optimizations :none
                                   :process-shim false}}]}
  :aliases {"test-cljs" ["doo" "node" "test-cljs" "once"]
            "test-cljs-auto" ["doo" "node" "test-cljs" "auto"]
            "test-all" ["do" ["test"] ["test-cljs"]]})
