(ns camelot.market.config
  "Configuration management for Camelot."
  (:require [camelot.market.impl.io :as io]
            [camelot.market.impl.constants :as constants]
            [camelot.market.impl.config :as config]
            [camelot.market.impl.paths :as paths]
            [camelot.market.spec :as spec]
            [clojure.spec.alpha :as s]
            [clojure.set :as cset]))

(defn- spec-problems
  [config]
  (some->> config
           (s/explain-data ::spec/config)
           ::s/problems
           (mapv :path)
           set))

(defn- path-problems
  [config]
  (->> config
       :paths
       paths/validate-paths
       (map #(vector :paths %))))

(defn- flatten-paths
  [config]
  (mapcat (fn [[ds val]]
            (map (fn [[k p]]
                      (vector [:datasets ds :paths k] p))
                    (:paths val)))
          (:datasets config)))

(defn- dataset-problems
  [config]
  (let [paths (flatten-paths config)
        m (group-by second paths)
        dups (->> paths
                  (map second)
                  frequencies
                  (filter (fn [[k v]] (> v 1)))
                  (map first))]
    (set (mapcat (comp #(map first %) m) dups))))

(defn- detector-problems
  [config]
  (let [d (:detector config)]
    (when (:enabled d)
      (->> [:username :password]
           (remove #(> (count (get d % "")) 0))
           (into #{})
           (map #(vector :detector %))))))

(defn validate-config
  "Validate the passed configuration, returning a set of paths to all entries
  which do not pass the validation."
  [config]
  (cset/union (spec-problems config)
              (path-problems config)
              (detector-problems config)
              (dataset-problems config)))

(defn write-config!
  "Save the configuration data.
  Overwrites the configuration file if the `overwrite?' flag is set.

  Throws should an invalid configuration be passed."
  ([config]
   (write-config! config {}))
  ([config {:keys [overwrite? skip-validation?] :or {overwrite? true}}]
   (let [validation (if skip-validation? [] (validate-config config))]
     (when (seq validation)
       (throw (ex-info "The given configuration is not valid"
                       {:problem-paths validation
                        :config config}))))

   (let [cf (paths/get-config-file)]
     (if (and (not overwrite?) (io/exists? cf))
       (throw (ex-info "A configuration file already exists." {}))
       (config/write-config! cf (config/dehydrate config))))))

(defn read-config
  []
  (let [cf (paths/get-config-file)
        raw-config (config/read-config cf)
        upgraded-config (config/upgrade raw-config)]
    (when (or (not (io/exists? cf))
              (not= raw-config upgraded-config))
      (write-config! upgraded-config {:overwrite true
                                      :skip-validation? true}))
    (let [config (config/hydrate upgraded-config)]
      (paths/initialise-paths (:paths config))
      (when-let [default-paths (get-in config [:datasets constants/default-dataset-id :paths])]
        (paths/initialise-paths default-paths))
      config)))

(s/fdef read-config
        :args (s/cat)
        :ret ::spec/config)

(defn reset-config!
  "Reset to the default configuration."
  []
  (let [default-config (config/get-default-config)]
    (write-config! default-config)
    default-config))

(s/def ::overwrite? boolean?)
(s/def ::write-config-options
  (s/keys :opt-un []))

(s/fdef write-config!
        :args (s/or :with-options (s/cat :config ::spec/config
                                         :opts ::write-config-options)
                    :default-options (s/cat :config ::spec/config))
        :ret ::spec/config)

(def get-default-dataset-paths paths/get-default-dataset-paths)
