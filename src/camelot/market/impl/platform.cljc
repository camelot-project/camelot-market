(ns camelot.market.impl.platform
  "Cross-platform abstractions."
  (:require
   [camelot.market.impl.io :as io]
   [camelot.market.impl.util :as util]
   [camelot.market.impl.environ :refer [env]])
  #?(:clj (:import
           (org.apache.commons.lang3 SystemUtils))))

#?(:cljs
   (def ^:private os (js/require "os")))

(defn get-os
  "Return a key representing the OS Camelot is running upon."
  []
  #?(:clj (cond
            SystemUtils/IS_OS_WINDOWS :windows
            SystemUtils/IS_OS_LINUX :linux
            SystemUtils/IS_OS_MAC_OSX :macos
            :else :other)
     :cljs (let [ps {"linux" :linux
                     "win32" :windows
                     "darwin" :macos}]
             (get ps (.platform os) :other))))

(defn base-data-dir
  []
  (let [d (case (get-os)
            :windows (env :localappdata)
            :linux (io/file (env :home) ".local" "share")
            :macos (io/file (env :home) "Library" "Application Support")
            :other ".")]
    (util/initialise-dir d)
    (util/assert-dir d)))

(defn base-config-dir
  []
  (let [d (case (get-os)
            :windows (env :appdata)
            :linux (io/file (env :home) ".config")
            :macos (io/file (env :home) "Library" "Preferences")
            :other ".")]
    (util/initialise-dir d)
    (util/assert-dir d)))
