(ns camelot.market.impl.environ
  "Environment wrapper. Can be removed once environ gets cljs support."
  (:require
   #?(:clj [environ.core :as environ])
   #?(:cljs [goog.object :as obj])
   #?(:cljs [clojure.string :as cstr])))

#?(:cljs
   (def process (js/require "process")))

#?(:cljs
   (defn- keywordize [s]
     (-> (cstr/lower-case s)
         (cstr/replace "_" "-")
         (cstr/replace "." "-")
         keyword)))

#?(:cljs
   (defn- lookup-key
     [k]
     (let [m (->> (obj/get process "env")
                  obj/getKeys
                  (map #(vector (keywordize %) %))
                  (into {}))]
       (m k))))

(defn env
  [k]
  #?(:clj (environ/env k)
     :cljs (obj/get (obj/get process "env") (lookup-key k))))
