(ns camelot.market.impl.paths
  "Filesystem abstractions for configuration and data paths."
  (:require
   [camelot.market.impl.io :as io]
   [camelot.market.impl.util :as util]
   [camelot.market.impl.constants :as constants]
   [camelot.market.impl.platform :as platform]
   [camelot.market.impl.environ :refer [env]]))

(defn init-and-assert-dir
  [d]
  (util/initialise-dir d)
  (util/assert-dir d))

(defn- own-dir
  [dir]
  (-> dir
      init-and-assert-dir
      (io/file "camelot")
      init-and-assert-dir))

(defn- get-config-dir
  "Return the OS-specific path to the config directory."
  []
  (own-dir (platform/base-config-dir)))

(defn get-config-file
  "Return the OS-specific path to the config file."
  []
  (io/file (get-config-dir) constants/config-filename))

(defn get-legacy-config-file
  "Return the OS-specific path to the config file."
  []
  (io/file (get-config-dir) constants/legacy-config-filename))

(defn- get-default-data-dir
  "Return the OS-specific path to the data directory."
  []
  (or (env :camelot-datadir)
      (own-dir (platform/base-data-dir))))

(defn get-historic-paths
  "Return a map of paths for prior releases."
  [version]
  (condp = version
    :v1.5.x
    (let [dd (get-default-data-dir)]
      {:media (io/file dd constants/media-directory-name)
       :filestore-base (io/file dd constants/filestore-directory-name)
       :backup (io/file dd constants/backup-directory-name)
       :database dd})))

(defn initialise-paths
  [paths]
  (doall (->> paths
              vals
              (map util/initialise-dir)))
  paths)

(defn path-validators
  []
  {:media util/assert-parent-dir
   :filestore-base util/assert-parent-dir
   :backup util/assert-parent-dir
   :database util/assert-parent-dir
   :log util/assert-parent-dir
   :config util/assert-parent-dir
   :root util/assert-dir
   :application util/assert-dir})

(defn validate-path
  [k path]
  (if-let [validator ((path-validators) k)]
    (validator path)
    path))

(defn validate-paths
  [paths]
  (letfn [(reducer [acc k v]
            (try
              (validate-path k v)
              acc
              (catch #?(:clj Exception :cljs js/Error) _
                (conj acc k))))]
    (reduce-kv reducer #{} paths)))

(defn- get-dataset-dir
  [dataset]
  (-> (get-default-data-dir)
      (io/file "Datasets")
      init-and-assert-dir
      (io/file (name dataset))
      init-and-assert-dir))

(defn get-default-dataset-paths
  "Return the default dataset paths"
  [paths dataset]
  (let [dd (get-dataset-dir dataset)]
    {:media (io/file dd constants/media-directory-name)
     :filestore-base (io/file dd constants/filestore-directory-name)
     :backup (io/file (:backup paths) (name dataset))
     :database dd}))

(defn get-default-application-paths
  "Return the default application paths"
  []
  (let [dd (get-default-data-dir)]
    {:log (io/file dd constants/log-directory-name)
     :config (get-config-dir)
     :backup (io/file dd constants/backup-directory-name)
     :root (io/user-dir)
     :application "."}))
