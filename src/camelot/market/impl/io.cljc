(ns camelot.market.impl.io
  (:refer-clojure :exclude [spit slurp exists?])
  (:require
   #?(:clj [clojure.java.io :as io])
   #?(:cljs [cljs-node-io.core :as node-io]))
  #?(:clj (:import (java.io File)
                   (java.nio.file Files StandardCopyOption))))

#?(:cljs
   (def ^:private os (js/require "os")))
#?(:cljs
   (def ^:private fs (js/require "fs")))
#?(:cljs
   (def ^:private path (js/require "path")))

(defn exists?
  [f]
  #?(:clj (.exists (io/file f)))
  #?(:cljs (.existsSync fs f)))

(defn mkdirs
  [dir]
  #?(:clj (.mkdirs (io/file dir))
     :cljs (do (.mkdirSync fs dir #js {"recursive" true})
               true)))

(defn directory?
  [f]
  #?(:clj (.isDirectory (io/file f))
     :cljs (.isDirectory (.statSync fs f))))

(defn delete
  [f]
  #?(:clj (Files/deleteIfExists (.toPath (io/file f)))
     :cljs (.unlinkSync fs f)))

(defn file
  ([dir f]
   #?(:clj (.getPath (io/file dir f)))
   #?(:cljs (.join path dir f)))
  ([dir f1 f2]
   #?(:clj (.getPath (io/file dir f1 f2)))
   #?(:cljs (.join path dir f1 f2))))

(defn readable?
  [f]
  #?(:clj (.canRead (io/file f))
     ;; TODO
     :cljs true))

(defn writable?
  [f]
  #?(:clj (.canWrite (io/file f))
     ;; TODO
     :cljs true))

#?(:cljs
   (defn- random-string
     []
     (apply str (repeatedly 2 #(.substring (.toString (.random js/Math) 36) 2 15)))))

(defn create-temp-file
  [prefix suffix]
  #?(:clj  (.getPath (File/createTempFile prefix suffix))
     :cljs (file (.tmpdir os) (str prefix "-" (random-string) suffix))))

(defn rename
  [source dest]
  #?(:clj (let [sf (io/file source)
                df (io/file dest)]
            (or (.renameTo sf df)
                (do
                  (Files/copy (.toPath sf) (.toPath df)
                              (into-array StandardCopyOption
                                          [StandardCopyOption/REPLACE_EXISTING]))
                  (.delete sf))))
     :cljs (do (try
                 (.renameSync fs source dest)
                 ;; renameSync throws when copying across FS boundaries.
                 (catch js/Error e
                   (.copyFileSync fs source dest)
                   (.unlinkSync fs source)))
              true)))

(defn parent
  [f]
  #?(:clj (.getParent (io/file f))
     :cljs (.dirname path f)))

(defn user-dir
  []
  #?(:clj (System/getProperty "user.home")
     :cljs (.homedir os)))

(defn slurp
  [f]
  #?(:clj (clojure.core/slurp f)
     :cljs (node-io/slurp f)))

(defn spit
  [f data]
  #?(:clj (clojure.core/spit f data)
     :cljs (node-io/spit f data)))
