(ns camelot.market.impl.util
  "Configuration and filesystem utilities."
  (:require [camelot.market.impl.io :as io]))

(defn initialise-dir
  "Create a directory should it not already exist.
  Returns true if the directory was created or already exists. False otherwise."
  [dir]
  (try
    (when (not (io/exists? dir))
      (io/mkdirs dir))
    (catch #?(:clj Exception
              :cljs js/Error) _
      false)))

(defn assert-dir
  "Ensure the OS-level default directory has the expected access."
  [dir]
  (cond
    (not (io/exists? dir))
    (throw (ex-info "Directory does not exist" {:dir dir}))

    (not (io/directory? dir))
    (throw (ex-info "File is not a directory" {:dir dir}))

    (not (io/readable? dir))
    (throw (ex-info "Directory is not readable" {:dir dir}))

    (not (io/writable? dir))
    (throw (ex-info "Directory is not writable" {:dir dir}))

    :else
    dir))

(defn assert-parent-dir
  "Ensure the parent of the given directory exists and has the expected access."
  [dir]
  (assert-dir (io/parent dir))
  dir)

(defn deep-merge
  "Merge maps in `ms` recursively"
  [& ms]
  (apply merge-with (fn [x y]
                      (if (map? y)
                        (deep-merge x y)
                        y))
         ms))

(defn diff-hashmaps
  "Return a, less those k-v pairs identical to b. Walks maps recursively."
  [a b]
  (reduce-kv (fn [acc ak av]
               (let [bv (get b ak)]
                 (cond
                   (and (map? bv) (map? av))
                   (let [nv (diff-hashmaps av bv)]
                     (if (seq nv)
                       (assoc acc ak nv)
                       acc))

                   (not= av bv)
                   (assoc acc ak av)

                   :else acc)))
             {} a))
