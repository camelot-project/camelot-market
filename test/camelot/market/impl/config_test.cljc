(ns camelot.market.impl.config-test
  (:require [camelot.market.impl.io :as io]
            [camelot.market.impl.paths :as paths]
            [camelot.market.impl.config :as sut]
            #?(:clj [clojure.test :refer [testing deftest is]]
               :cljs [cljs.test :refer [testing is deftest]])))

(deftest test-read-config
  (testing "read-config"
    (with-redefs [io/slurp (constantly "{\"some-key\": true}")
                  io/exists? (constantly true)
                  paths/get-legacy-config-file (constantly "config.clj")]

      (testing "should read config data as JSON"
        (is (= {:some-key true}
               (sut/read-config "config.json"))))

      (testing "should transform keys recursively"
        (with-redefs [io/slurp (constantly "{\"some-key\": {\"sub-key\": true}}")]
          (is (= {:some-key {:sub-key true}}
                 (sut/read-config "config.json")))))

      (testing "should transform keys recursively"
        (with-redefs [io/slurp (constantly "{\"some-key\": {\"sub-key\": true}}")]
          (is (= {:some-key {:sub-key true}}
                 (sut/read-config "config.json")))))

      (testing "should transform keys nested within arrays recursively"
        (with-redefs [io/slurp (constantly "{\"some-key\": [{\"sub-key\": true}]}")]
          (is (= {:some-key [{:sub-key true}]}
                 (sut/read-config "config.json")))))

      (testing "should transform content within a nested array"
        (with-redefs [io/slurp (constantly "{\"some-key\": [[\"a string\"]]}")]
          (is (= {:some-key [["a string"]]}
                 (sut/read-config "config.json")))))

      (testing "should convert the value of the language key to a keyword"
        (with-redefs [io/slurp (constantly "{\"language\": \"en\"}")]
          (is (= {:language :en}
                 (sut/read-config "config.json")))))

      (testing "should convert the value of the launcher ui-mode key to a keyword"
        (with-redefs [io/slurp (constantly "{\"launcher\": {\"ui-mode\": \"cli\"}}")]
          (is (= {:launcher {:ui-mode :cli}}
                 (sut/read-config "config.json")))))

      (testing "should convert the value of the species-name-style key to a keyword"
        (with-redefs [io/slurp (constantly "{\"species-name-style\": \"common-name\"}")]
          (is (= {:species-name-style :common-name}
                 (sut/read-config "config.json")))))

      (testing "should leave the value of other strings alone to a keyword"
        (with-redefs [io/slurp (constantly "{\"jvm-extra-args\": \"-server\"}")]
          (is (= {:jvm-extra-args "-server"}
                 (sut/read-config "config.json")))))

      (testing "should return nil if config and legacy config files do not exist"
        (with-redefs [io/exists? (constantly false)]
          (is (nil? (sut/read-config "config.json")))))

      (testing "configuration upgrade"
        (testing "should upgrade from the v1 shape to the latest shape"
          (with-redefs [io/exists? #(not (= % "config.json"))
                        io/slurp (constantly "{:jvm-extra-args \"-server\"
:http-port 5341
:media-importers 8
:max-heap-size \"1024m\"
:java-command \"java\"}")]
            (is (= {:server {:jvm-extra-args "-server"
                             :http-port 5341
                             :max-heap-size "1024m"
                             :media-importers 8}
                    :java-command "java"}
                   (sut/read-config "config.json")))))))))

(deftest test-write-config!
  (testing "write-config!"
    (with-redefs [io/delete (constantly nil)
                  paths/get-legacy-config-file (constantly "config.clj")
                  io/create-temp-file (constantly "some-file")
                  io/rename (constantly true)]

      (testing "should write config to temp file"
        (let [write-args (atom nil)]
          (with-redefs [io/spit (fn [& args] (reset! write-args args))]
            (sut/write-config! "config.json" {:some-key true})
            (is (= ["some-file" "{\"some-key\":true}"] @write-args)))))

      (testing "should throw if rename fails"
        (with-redefs [io/rename (constantly false)
                      io/spit (comp second list)]
          (is (thrown-with-msg? #?(:clj RuntimeException :cljs js/Error)
                                #"Failed to rename temporary configuration file"
                                (sut/write-config! "config.json" {:some-key true})))))

      (testing "should transform keys recursively"
        (let [write-args (atom nil)]
          (with-redefs [io/spit (fn [& args] (reset! write-args args))]
            (sut/write-config! "config.json" {:some-key {:sub-key true}})
            (is (= "{\"some-key\":{\"sub-key\":true}}" (second @write-args))))))

      (testing "should transform keys nested within arrays recursively"
        (let [write-args (atom nil)]
          (with-redefs [io/spit (fn [& args] (reset! write-args args))]
            (sut/write-config! "config.json" {:some-key [{:sub-key true}]})
            (is (= "{\"some-key\":[{\"sub-key\":true}]}" (second @write-args)))))))))

(def gen-client-id (constantly "123"))

(defn- upgrade
  [c]
  (sut/upgrade c :client-id-generator gen-client-id))

(def default-paths {:media "/default/path/to/media"
                    :database "/default/path/to"
                    :filestore-base "/default/path/to/filestore"
                    :backups "/default/path/to/backups"})

(deftest test-upgrade
  (testing "upgrade"
    (with-redefs [paths/get-historic-paths (constantly default-paths)]
      (testing "should return expected top-level keys"
        (let [config {:paths {:root "/path/to/root"
                              :database "/path/to/dataset/database"}
                      :other-config true}]
          (is (= #{:paths :datasets :other-config :client-id}
                 (set (keys (upgrade config)))))))

      (testing "upgraded dataset paths"
        (testing "should preserve application path configuration"
          (let [config {:paths {:media "/path/to/media"
                                :logs "/path/to/logs"}}]
            (is (= {:datasets {:default {:paths {:media "/path/to/media"
                                                 :database "/default/path/to"
                                                 :filestore-base "/default/path/to/filestore"
                                                 :backups "/default/path/to/backups"}}}
                    :paths {:logs "/path/to/logs"}}
                   (select-keys (upgrade config) [:datasets :paths])))))

        (testing "should upgrade path configuration"
          (let [config {:paths {:media "/path/to/media"}}]
            (is (= {:default {:paths (assoc default-paths :media "/path/to/media")}}
                   (:datasets (upgrade config))))))

        (testing "should remove path configuration"
          (let [config {:paths {:media "/path/to/media"
                                :root "/path/to/root"}}]
            (is (= {:root "/path/to/root"} (:paths (upgrade config))))))

        (testing "should preserve other datasets"
          (let [config {:datasets {:special {:paths {:media "/path/to/special/media"}}}
                        :paths {:media "/path/to/media"}}]
            (is (= {:special {:paths {:media "/path/to/special/media"}}}
                   (:datasets (upgrade config))))))))

    (testing "client-id upgrade"
      (testing "should generate a client-id if none is present"
        (is (= "123" (:client-id (upgrade {})))))

      (testing "should not generate a new client-id if one is present"
        (is (= "42" (:client-id (upgrade {:client-id "42"})))))

      (testing "should use a random client-id generator if one is not given"
        (let [client-ids (repeatedly 10 #(:client-id (sut/upgrade {})))]
          (is (= (count (distinct client-ids)) (count client-ids))
              "Every generated client-id should be unique"))))

    (testing "ran for the initial configuration"
      (testing "should produce the expected result"
        (is (= {:datasets {:default {}}
                :client-id "123"}
               (upgrade nil)))))))

(deftest test-get-default-config
  (with-redefs [paths/get-default-data-dir (constantly "/homedir")
                paths/get-dataset-dir (fn [d] (str "/homedir/dataset/" (name d)))
                paths/get-config-dir (constantly "/configdir")
                io/user-dir (constantly "/user-dir")]
    (testing "get-default-config"
      (is (= {:datasets
              {:default
               {:name "Default",
                :paths {:media "/homedir/dataset/default/Media",
                        :filestore-base "/homedir/dataset/default/FileStore",
                        :backup "/homedir/Backups/default",
                        :database "/homedir/dataset/default"}}},
              :paths {:log "/homedir/Logs",
                      :config "/configdir",
                      :backup "/homedir/Backups"
                      :root "/user-dir",
                      :application "."},
              :detector {:enabled false,
                         :api-url "https://api.prod.camelotproject.org",
                         :confidence-threshold 0.9}
              :open-browser-on-startup true,
              :server {:http-port 5341, :media-importers 4, :jvm-extra-args ""},
              :java-command "java",
              :language :en,
              :dev-mode false,
              :species-name-style :scientific,
              :send-usage-data false}
             (dissoc (sut/get-default-config) :client-id))))))

(deftest test-dehydrate
  (testing "dehydrate"
    (with-redefs [io/exists? (constantly true)
                  paths/get-default-application-paths (constantly {:root "/path/to/root"})
                  paths/get-default-dataset-paths (constantly {:database "/path/to/database"})]
      (testing "should elide default configuration"
        (is (= {:datasets {}} (sut/dehydrate {:server {:http-port 5341}}))))

      (testing "should elide default dataset configuration"
        (let [config {:datasets {:default {:paths {:database "/path/to/database"}}}}]
          (is (= {:datasets {:default {}}}
                 (sut/dehydrate config)))))

      (testing "should elide alternate dataset configuration when it matches defaults"
        (let [config {:datasets {:other {:paths {:database "/path/to/database"}}}}]
          (is (= {:datasets {:other {}}}
                 (sut/dehydrate config)))))

      (testing "should retain alternate dataset configuration which is non-default"
        (let [config {:datasets {:other {:paths {:database "/special/path/to/database"}}}}]
          (is (= {:datasets {:other {:paths {:database "/special/path/to/database"}}}}
                 (sut/dehydrate config))))))))
