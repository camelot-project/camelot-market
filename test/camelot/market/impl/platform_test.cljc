(ns camelot.market.impl.platform-test
  (:require [camelot.market.impl.platform :as sut]
            #?(:clj [clojure.test :refer [testing is deftest]]
               :cljs [cljs.test :refer [testing is deftest]])
            [camelot.market.impl.environ :as environ]
            [camelot.market.impl.util :as util]))

(defn fake-env
  [k]
  (condp = k
    :home "/home/user"
    :localappdata "C:\\User\\local\\appdata"
    :appdata "C:\\User\\appdata"))

(deftest test-base-data-dir
  (testing "base-data-dir"
    (with-redefs [environ/env fake-env
                  util/initialise-dir (constantly true)]
      (testing "returned path"
        (with-redefs [util/assert-dir (comp identity first list)
                      environ/env fake-env]

          (testing "should be the expected path on linux"
            (with-redefs [sut/get-os (constantly :linux)]
              (is (= "/home/user/.local/share" (sut/base-data-dir)))))

          (testing "should be the expected path on windows"
            (with-redefs [sut/get-os (constantly :windows)]
              (is (= "C:\\User\\local\\appdata" (sut/base-data-dir)))))

          (testing "should be the expected path on macos"
            (with-redefs [sut/get-os (constantly :macos)]
              (is (= "/home/user/Library/Application Support"
                     (sut/base-data-dir)))))

          (testing "should be the current directory on other OSs"
            (with-redefs [sut/get-os (constantly :other)]
              (is (= "."
                     (sut/base-data-dir)))))))

      (testing "assertions"
        (testing "should throw if assert-dir is unhappy"
          (with-redefs [util/assert-dir
                        (fn [_] (throw (ex-info "Assertion failed" {})))]
            (is (thrown? #?(:clj RuntimeException :cljs js/Error) (sut/base-data-dir)))))))))

(deftest test-base-config-dir
  (testing "base-config-dir"
    (with-redefs [environ/env fake-env
                  util/initialise-dir (constantly true)]
      (testing "returned path"
        (with-redefs [util/assert-dir (comp identity first list)]

          (testing "should be the expected path on linux"
            (with-redefs [sut/get-os (constantly :linux)]
              (is (= "/home/user/.config" (sut/base-config-dir)))))

          (testing "should be the expected path on windows"
            (with-redefs [sut/get-os (constantly :windows)]
              (is (= "C:\\User\\appdata" (sut/base-config-dir)))))

          (testing "should be the expected path on macos"
            (with-redefs [sut/get-os (constantly :macos)]
              (is (= "/home/user/Library/Preferences"
                     (sut/base-config-dir)))))

          (testing "should be the current directory on other OSs"
            (with-redefs [sut/get-os (constantly :other)]
              (is (= "."
                     (sut/base-config-dir))))))))

    (testing "assertions"
      (testing "should throw if assert-dir is unhappy"
        (with-redefs [util/assert-dir
                      (fn [_] (throw (ex-info "Assertion failed" {})))]
          (is (thrown? #?(:clj RuntimeException :cljs js/Error) (sut/base-config-dir))))))))
