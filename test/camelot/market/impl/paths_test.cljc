(ns camelot.market.impl.paths-test
  (:require [camelot.market.impl.paths :as sut]
            #?(:clj [clojure.test :refer [testing deftest is]]
               :cljs [cljs.test :refer [testing deftest is]])
            [camelot.market.impl.io :as io]
            [camelot.market.impl.platform :as platform]
            [camelot.market.impl.util :as util]
            [camelot.market.impl.environ :refer [env]]))

(deftest test-get-config-file
  (testing "get-config-file"
    (testing "should return the expected file"
      (with-redefs [platform/base-config-dir
                    (constantly "/path/to/.config")
                    util/assert-dir identity
                    util/assert-parent-dir identity]
        (is (= "/path/to/.config/camelot/config.json" (sut/get-config-file)))))

    (testing "should assert that the base directory has the expected properties"
      (with-redefs [platform/base-config-dir
                    (constantly "/path/to/.config")
                    util/assert-dir
                    (fn [f]
                      (is (= f "/path/to/.config"))
                      (throw (ex-info "Assertion failed" {})))
                    util/assert-parent-dir identity]
        (is (thrown-with-msg? #?(:clj RuntimeException :cljs js/Error)
                              #"Assertion failed"
                              (sut/get-config-file)))))

    (testing "should assert the camelot directory has the expected properties"
      (with-redefs [platform/base-config-dir
                    (constantly "/path/to/.config")
                    util/assert-dir
                    (fn [f]
                      (throw (ex-info "Assertion failed" {}))
                      f)
                    util/assert-parent-dir identity]
        (is (thrown-with-msg? #?(:clj RuntimeException :cljs js/Error)
                              #"Assertion failed"
                              (sut/get-config-file)))))))

(defn expected-application-paths
  [base]
  ;; TODO these won't pass under windows. At least, they'd better not!
  {:log (str base "/Logs")
   :config "/home/user/.config/camelot"
   :root "/home/user"
   :backup (str base "/Backups")
   :application "."})

(deftest test-get-default-application-paths
  (testing "get-default-application-paths"
    (with-redefs [platform/base-config-dir (constantly "/home/user/.config")
                  platform/base-data-dir (constantly "/path/to/data")
                  util/assert-dir (comp identity first list)
                  util/assert-parent-dir identity
                  io/user-dir (constantly "/home/user")]
      (testing "should return the expected paths"
        (is (= (expected-application-paths "/path/to/data/camelot")
               (sut/get-default-application-paths))))

      (testing "should respect CAMELOT_DATADIR"
        (with-redefs [env (fn [k] (when (= :camelot-datadir k)
                                            "/datadir"))]
          (is (= (expected-application-paths "/datadir")
                 (sut/get-default-application-paths)))))

      (testing "should assert that the base directory has the expected properties"
        (with-redefs [util/assert-dir
                      (fn [f]
                        (when-not (= f "/path/to/data")
                          (throw (ex-info "Assertion failed" {}))))]
          (is (thrown-with-msg? #?(:clj RuntimeException :cljs js/Error)
                                #"Assertion failed"
                                (sut/get-config-file)))))

    (testing "should assert the camelot directory has the expected properties"
      (with-redefs [platform/base-config-dir
                    (constantly "/path/to/data")
                    util/assert-dir
                    (fn [f]
                      (if (= f "/path/to/data/camelot")
                        (throw (ex-info "Assertion failed" {})))
                      f)
                    util/assert-parent-dir identity]
        (is (thrown-with-msg? #?(:clj RuntimeException :cljs js/Error)
                              #"Assertion failed"
                              (sut/get-config-file))))))))

(defn expected-dataset-paths
  [base]
  ;; TODO these won't pass under windows. At least, they'd better not!
  {:media (str base "/Datasets/default/Media")
   :filestore-base (str base "/Datasets/default/FileStore")
   :backup (str base "/Backups/default")
   :database (str base "/Datasets/default")})

(deftest test-get-default-dataset-paths
  (testing "get-default-dataset-paths"
    (with-redefs [platform/base-config-dir (constantly "/home/user/.config")
                  platform/base-data-dir (constantly "/path/to/data/")
                  util/assert-dir (comp identity first list)
                  util/assert-parent-dir identity
                  io/user-dir (constantly "/home/user")]
      (testing "should return the expected paths"
        (is (= (expected-dataset-paths "/path/to/data/camelot")
               (sut/get-default-dataset-paths (expected-application-paths "/path/to/data/camelot")
                                              :default))))

      (testing "should respect CAMELOT_DATADIR"
        (with-redefs [env (fn [k] (when (= :camelot-datadir k)
                                            "/datadir"))]
          (is (= (expected-dataset-paths "/datadir")
                 (sut/get-default-dataset-paths (expected-application-paths "/datadir")
                                                :default)))))

      (testing "should assert that the base directory has the expected properties"
        (with-redefs [util/assert-dir
                      (fn [f]
                        (when-not (= f "/path/to/data")
                          (throw (ex-info "Assertion failed" {}))))]
          (is (thrown-with-msg? #?(:clj RuntimeException :cljs js/Error)
                                #"Assertion failed"
                                (sut/get-config-file)))))

    (testing "should assert the camelot directory has the expected properties"
      (with-redefs [platform/base-config-dir
                    (constantly "/path/to/data")
                    util/assert-dir
                    (fn [f]
                      (if (= f "/path/to/data/camelot")
                        (throw (ex-info "Assertion failed" {})))
                      f)
                    util/assert-parent-dir identity]
        (is (thrown-with-msg? #?(:clj RuntimeException :cljs js/Error)
                              #"Assertion failed"
                              (sut/get-config-file))))))))

(deftest test-initialise-paths
  (testing "initialise-paths"
    (testing "should initialise the expected paths"
      (let [calls (atom [])]
        (with-redefs [util/initialise-dir #(swap! calls conj %)]
          (let [paths {:media "/path/to/camelot/Media"
                       :database "/path/to/camelot"}
                expected-paths #{"/path/to/camelot/Media"
                                 "/path/to/camelot"}]
            (sut/initialise-paths paths)
            (is (= expected-paths (set @calls)))))))

    (testing "should return the expected paths"
      (with-redefs [util/initialise-dir (constantly true)]
        (let [paths {:media "/path/to/camelot/Media"
                     :database "/path/to/camelot"}]
          (is (= paths (sut/initialise-paths paths))))))))

(defn mock-assert-parent-dir
  [_]
  :validated-parent-dir)

(defn mock-assert-dir
  [_]
  :validated-dir)

(deftest test-validate-paths
  (testing "validate-paths"
    (testing "should validate the parents of the expected paths"
      (let [calls (atom [])]
        (with-redefs [util/assert-parent-dir #(swap! calls conj %)
                      util/assert-dir mock-assert-dir]
          (let [paths {:media "/path/to/camelot/Media"
                      :filestore-base "/path/to/camelot/FileStore"
                       :database "/path/to/camelot"
                      :log "/path/to/camelot/Logs"
                      :backup "/path/to/camelot/Backups"
                      :config "/path/to/camelot/config.json"
                      :root "/path/to/home"
                      :application "/path/to/camelot-version"}
                expected #{"/path/to/camelot/Media"
                           "/path/to/camelot/FileStore"
                           "/path/to/camelot"
                           "/path/to/camelot/Logs"
                           "/path/to/camelot/Backups"
                           "/path/to/camelot/config.json"}]
            (sut/validate-paths paths)
            (is (= expected (set @calls)))))))

    (testing "should validate the directories of the expected paths"
      (let [calls (atom [])]
        (with-redefs [util/assert-parent-dir mock-assert-parent-dir
                      util/assert-dir #(swap! calls conj %)]
          (let [paths {:media "/path/to/camelot/Media"
                       :filestore-base "/path/to/camelot/FileStore"
                       :database "/path/to/camelot"
                       :log "/path/to/camelot/Logs"
                       :backup "/path/to/camelot/Backup"
                       :config "/path/to/camelot/config.json"
                       :root "/path/to/home"
                       :application "/path/to/camelot-version"}
                expected #{"/path/to/home"
                           "/path/to/camelot-version"}]
            (sut/validate-paths paths)
            (is (= expected (set @calls)))))))

    (testing "should return any key failing validation"
      (with-redefs [util/assert-parent-dir #(when (= % "/path/to/camelot/Media")
                                              (throw (ex-info "Error" {})))
                    util/assert-dir #(when (= % "/path/to/camelot-version")
                                       (throw (ex-info "Error" {})))]
        (let [paths {:media "/path/to/camelot/Media"
                     :filestore-base "/path/to/camelot/FileStore"
                     :backup "/path/to/camelot/Backup"
                     :config "/path/to/camelot/config.json"
                     :root "/path/to/home"
                     :application "/path/to/camelot-version"
                     :other "/something"}
              expected #{:media :application}]
          (is (= expected (sut/validate-paths paths))))))))
